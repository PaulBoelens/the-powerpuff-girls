/**
 * Remove all character sequences that start with < and end with >
 */
export const sanitizeText = richText =>
  richText ? richText.replace(/<.+?>/gm, '') : '';

/**
 * Sanitize data fetched from the TVMaze API
 */
export const sanitizeAPIData = ({
  id,
  name,
  number,
  season,
  language,
  status,
  schedule,
  summary,
  image,
}) => ({
  name,
  number,
  season,
  language,
  status,
  id: +id,
  summary: sanitizeText(summary),
  imageOriginal: image && image.original,
  imageMedium: image && image.medium,
  schedule:
    schedule &&
    `${schedule.time}${(schedule.days.length && ` ${schedule.days[0]}`) || ''}`,
});
