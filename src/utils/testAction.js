/**
 * Test actions
 * @param {string} type - Action type
 * @param {string} action - Function that is related to the action type
 * @param {Object} data - JSON Object with data that is used by the action and state
 */
export default (type, action, data = []) => {
  it(`should succeed: ${type}`, () => {
    const expectedResponse = { type, ...data };
    const response = action(...Object.values(data));
    expect(response).toEqual(expectedResponse);
  });
};
