/**
 * Custom renderer for react-testing-library
 * Used to provide a store for the rendered component.
 */

import React from 'react';
import { render } from 'react-testing-library';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../reducers';
import rootMiddleware from '../middleware';

let localStore = null;

// Start with a clean store each step in a test
beforeEach(() => {
  localStore = createStore(
    rootReducer,
    {},
    compose(applyMiddleware(...rootMiddleware))
  );
});

const customRender = (node, store = localStore, ...options) =>
  render(<Provider store={store}>{node}</Provider>, ...options);

export * from 'react-testing-library';

export { customRender };
