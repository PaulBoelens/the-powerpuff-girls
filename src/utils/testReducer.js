/**
 * Test reducers
 */
export default class Test {
  /**
   * @param {function} reducer - Reducer that needs to be tested
   */
  constructor(reducer) {
    this.reducer = reducer;
  }

  /**
   * @param {function} action - Function that provides provides the data to the reducer
   * @param {Object} action - Expected outcome of the processed data.
   */
  run(action, expected) {
    const result = this.reducer(undefined, action);
    expect(result).toEqual(expected);
  }
}
