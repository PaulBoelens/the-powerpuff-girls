import React from 'react';
import { Route, MemoryRouter } from 'react-router';
import { customRender, waitForElement } from '../utils/testUtils';
import Episode from '../containers/Episode';
import '../utils/mobile-check';

import { episodesMock } from '../mock/api';

beforeEach(() => {
  fetch.resetMocks();
});

describe('Episode', () => {
  const renderComponent = ({ showId }) =>
    customRender(
      <MemoryRouter initialEntries={[`/episode/${showId}/1/1`]}>
        <Route path="/episode/:showId/:season/:episode">
          <Episode />
        </Route>
      </MemoryRouter>
    );

  it('should render with data', async () => {
    fetch.mockResponseOnce(JSON.stringify(episodesMock), { status: 200 });
    const { container, getByTestId } = renderComponent({
      showId: 6771,
    });

    const component = await waitForElement(() => getByTestId('episode'));

    expect(component).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
