import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './TelevisionShow.module.scss';
import EpisodeList from '../containers/EpisodeList';

class TelevisionShow extends Component {
  componentDidMount() {
    const { show, getShow } = this.props;
    if (!show) getShow();
  }

  render() {
    const { show } = this.props;
    if (!show) return null;

    const {
      name,
      language,
      status,
      summary,
      imageOriginal,
      imageMedium,
      schedule,
    } = show;

    return (
      <section className={styles.grid} data-testid="television-show">
        <h1 className={styles.title}>{name}</h1>
        <ul className={styles.info}>
          {language && <li>{`Language: ${language}`}</li>}
          {status && <li>{`Status: ${status}`}</li>}
          {schedule && <li>{`Schedule: ${schedule}`}</li>}
        </ul>
        <article className={styles.summary}>{summary}</article>
        <EpisodeList className={styles.episodes} />
        {(imageMedium || imageOriginal) && (
          <img
            className={styles.image}
            src={window.mobilecheck() ? imageMedium : imageOriginal}
            alt={name}
          />
        )}
      </section>
    );
  }
}

TelevisionShow.propTypes = {
  show: PropTypes.shape({
    name: PropTypes.string,
    summary: PropTypes.string,
    language: PropTypes.string,
    status: PropTypes.string,
    imageOriginal: PropTypes.string,
    imageMedium: PropTypes.string,
    schedule: PropTypes.string,
  }),
  getShow: PropTypes.func.isRequired,
};

export default TelevisionShow;
