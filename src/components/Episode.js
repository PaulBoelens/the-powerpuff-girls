import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Episode.module.scss';

class Episode extends Component {
  componentDidMount() {
    const { episode, getEpisodes } = this.props;
    if (!episode) getEpisodes();
  }

  render() {
    const { episode } = this.props;

    if (!episode) return <div>No episode available</div>;

    const { name, number, summary, imageMedium, imageOriginal } = episode;

    return (
      <section className={styles.wrapper} data-testid="episode">
        <h1>{name}</h1>
        <h2>{`Episode ${number}`}</h2>
        <p className={styles.text}>{summary || "'No summary available'"}</p>
        {(imageMedium || imageOriginal) && (
          <img
            className={styles.image}
            src={window.mobilecheck() ? imageMedium : imageOriginal}
            alt={name}
          />
        )}
      </section>
    );
  }
}

Episode.propTypes = {
  getEpisodes: PropTypes.func.isRequired,
  episode: PropTypes.shape({
    name: PropTypes.string,
    number: PropTypes.number,
    summary: PropTypes.string,
    imageOriginal: PropTypes.string,
    imageMedium: PropTypes.string,
  }),
};

export default Episode;
