import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './EpisodeItem.module.scss';

const EpisodeItem = ({ episodeNumber, title }) => (
  <li className={styles.listItem}>
    <Link
      className={styles.wrapper}
      to={`/episode/<shownumber>/${episodeNumber}`}
    >
      <div>
        <h4>{`Episode ${episodeNumber}`}</h4>
        <p>{title}</p>
      </div>
      <div className={styles.go}>{`>`}</div>
    </Link>
  </li>
);

EpisodeItem.propTypes = {
  episodeNumber: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

export default EpisodeItem;
