import React from 'react';
import { Route, MemoryRouter } from 'react-router';
import { customRender, waitForElement } from '../utils/testUtils';
import TelevisionShow from '../containers/TelevisionShow';
import '../utils/mobile-check';

import { televisionShowMock } from '../mock/api';

beforeEach(() => {
  fetch.resetMocks();
});

describe('TelevisionShow', () => {
  const renderComponent = ({ showId }) =>
    customRender(
      <MemoryRouter initialEntries={[`/${showId}`]}>
        <Route path="/:showId">
          <TelevisionShow />
        </Route>
      </MemoryRouter>
    );

  it('should render with data', async () => {
    fetch.mockResponseOnce(JSON.stringify(televisionShowMock), { status: 200 });
    const { container, getByTestId } = renderComponent({
      showId: televisionShowMock.id,
    });

    const component = await waitForElement(() =>
      getByTestId('television-show')
    );

    expect(component).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
