import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import '../utils/mobile-check';
import styles from './App.module.scss';

import TelevisionShow from '../containers/TelevisionShow';
import Episode from '../containers/Episode';

// TVMaze show id of 'The Powerpuff girls'
const defaultTelevisionShowId = 6771;

const App = () => (
  <Router>
    <main className={styles.main}>
      <Route
        exact
        path="/"
        render={() => <Redirect to={`/${defaultTelevisionShowId}`} />}
      />
      <Route exact path="/:showId" component={() => <TelevisionShow />} />
      <Route
        path="/episode/:showId/:season/:episode"
        component={() => <Episode />}
      />
    </main>
  </Router>
);

export default App;
