import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import styles from './EpisodeList.module.scss';

class EpisodeList extends Component {
  componentDidMount() {
    const { episodes, getEpisodes } = this.props;
    if (!episodes || !episodes.length) getEpisodes();
  }

  render() {
    const { className, episodes, showId } = this.props;

    if (!episodes || !episodes.length) return null;

    return (
      <ul
        className={classNames(className, styles.list)}
        data-testid="episode-list"
      >
        {episodes.map(episode => (
          <li
            key={episode.name}
            className={styles.listItem}
            data-testid={`episode-item-${episode.number}`}
          >
            <Link
              className={styles.wrapper}
              to={`/episode/${showId}/${episode.season}/${episode.number}`}
            >
              <div>
                <h4>{`Season ${episode.season} - Episode ${
                  episode.number
                }`}</h4>
                <p>{episode.name}</p>
              </div>
              <div className={styles.go}>{`>`}</div>
            </Link>
          </li>
        ))}
      </ul>
    );
  }
}

EpisodeList.propTypes = {
  getEpisodes: PropTypes.func.isRequired,
  showId: PropTypes.string.isRequired,
  episodes: PropTypes.arrayOf(PropTypes.object),
  className: PropTypes.string,
};

EpisodeList.defaultProps = {
  episodes: [],
};

export default EpisodeList;
