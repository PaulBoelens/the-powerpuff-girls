import { combineReducers } from 'redux';
import televisionShow from './televisionShow';
import episodes from './episodes';

export default combineReducers({
  televisionShow,
  episodes,
});
