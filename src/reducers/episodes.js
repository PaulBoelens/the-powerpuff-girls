import { REQUEST_EPISODES, RECEIVE_EPISODES } from '../actions/episodes';

import { sanitizeAPIData } from '../utils/sanitize';

export const initialState = {
  loading: false,
  list: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_EPISODES:
      return { ...state, loading: true };
    case RECEIVE_EPISODES: {
      const list = action.data
        ? [
            ...state.list,
            {
              episodes: action.data.map(data => sanitizeAPIData(data)),
              showId: +action.showId,
            },
          ]
        : state.list;
      return { ...state, loading: false, list };
    }
    default:
      return state;
  }
};
