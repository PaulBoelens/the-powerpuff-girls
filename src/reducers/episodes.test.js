import episodes, { initialState } from './episodes';
import { requestEpisodes, receiveEpisodes } from '../actions/episodes';
import Test from '../utils/testReducer';

describe('Episodes reducer', () => {
  const test = new Test(episodes);

  it('should succeed: request episodes', () => {
    test.run(requestEpisodes(6771), {
      ...initialState,
      loading: true,
    });
  });

  it('should succeed: receive television show: incomplete data', () => {
    const showId = 6771;
    const data = [
      {
        id: 4231,
        number: 3,
        season: 1,
        summary: 'foo bar',
      },
    ];

    const expectedEpisode = [
      {
        name: undefined,
        number: undefined,
        season: undefined,
        language: undefined,
        status: undefined,
        id: undefined,
        summary: undefined,
        imageOriginal: undefined,
        imageMedium: undefined,
        schedule: undefined,
        ...data[0],
      },
    ];

    test.run(receiveEpisodes(data, showId), {
      ...initialState,
      list: [
        {
          showId,
          episodes: expectedEpisode,
        },
      ],
      loading: false,
    });
  });
});
