import {
  REQUEST_TELEVISION_SHOW,
  RECEIVE_TELEVISION_SHOW,
} from '../actions/televisionShow';

import { sanitizeAPIData } from '../utils/sanitize';

export const initialState = {
  loading: false,
  list: [],
  error: undefined,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_TELEVISION_SHOW:
      return { ...state, loading: true };
    case RECEIVE_TELEVISION_SHOW: {
      const list = action.data
        ? [...state.list, sanitizeAPIData(action.data)]
        : state.list;

      return { ...state, loading: false, error: action.error, list };
    }
    default:
      return state;
  }
};
