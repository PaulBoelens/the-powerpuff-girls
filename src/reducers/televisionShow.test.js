import televisionShow, { initialState } from './televisionShow';
import { sanitizeAPIData } from '../utils/sanitize';
import {
  requestTelevisionShow,
  receiveTelevisionShow,
} from '../actions/televisionShow';
import Test from '../utils/testReducer';

import { televisionShowMock } from '../mock/api';

describe('TelevisionShow reducer', () => {
  const test = new Test(televisionShow);

  it('should succeed: request television show', () => {
    test.run(requestTelevisionShow(6771), {
      ...initialState,
      loading: true,
    });
  });

  it('should succeed: receive television show: complete data', () => {
    test.run(receiveTelevisionShow(televisionShowMock), {
      ...initialState,
      list: [sanitizeAPIData(televisionShowMock)],
      loading: false,
    });
  });

  it('should succeed: receive television show: incomplete data', () => {
    const data = {
      id: 6771,
      summary: 'foo bar',
    };

    test.run(receiveTelevisionShow(data), {
      ...initialState,
      list: [data],
      loading: false,
    });
  });
});
