export const REQUEST_EPISODES = 'REQUEST_EPISODES';
export const RECEIVE_EPISODES = 'RECEIVE_EPISODES';

export const requestEpisodes = showId => ({
  type: REQUEST_EPISODES,
  showId,
});

export const receiveEpisodes = (data, showId) => ({
  type: RECEIVE_EPISODES,
  data,
  showId,
});
