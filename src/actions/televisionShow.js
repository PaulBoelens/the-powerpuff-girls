export const REQUEST_TELEVISION_SHOW = 'REQUEST_TELEVISION_SHOW';
export const RECEIVE_TELEVISION_SHOW = 'RECEIVE_TELEVISION_SHOW';
export const REQUEST_EPISODES = 'REQUEST_EPISODES';
export const RECEIVE_EPISODES = 'RECEIVE_EPISODES';

export const requestTelevisionShow = id => ({
  type: REQUEST_TELEVISION_SHOW,
  id,
});

export const receiveTelevisionShow = (data, error) => ({
  type: RECEIVE_TELEVISION_SHOW,
  data,
  error,
});
