import {
  RECEIVE_EPISODES,
  REQUEST_EPISODES,
  receiveEpisodes,
  requestEpisodes,
} from './episodes';

import test from '../utils/testAction';

describe('Episodes actions', () => {
  test(REQUEST_EPISODES, requestEpisodes, { showId: 6771 });
  test(RECEIVE_EPISODES, receiveEpisodes, {
    data: { name: 'episode name' },
    showId: 6771,
  });
});
