import {
  RECEIVE_TELEVISION_SHOW,
  REQUEST_TELEVISION_SHOW,
  receiveTelevisionShow,
  requestTelevisionShow,
} from './televisionShow';

import test from '../utils/testAction';

describe('TelevisionShow actions', () => {
  test(REQUEST_TELEVISION_SHOW, requestTelevisionShow, { id: 6771 });
  test(RECEIVE_TELEVISION_SHOW, receiveTelevisionShow, { data: { id: 6771 } });
});
