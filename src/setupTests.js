import 'jest-dom/extend-expect';
import 'react-testing-library/cleanup-after-each';
import jestFetchMock from 'jest-fetch-mock';

global.fetch = jestFetchMock;
