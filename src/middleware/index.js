import televisionShow from './televisionShow';
import episodes from './episodes';

export default [televisionShow, episodes];
