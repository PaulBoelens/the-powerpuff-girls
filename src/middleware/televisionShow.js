import {
  REQUEST_TELEVISION_SHOW,
  receiveTelevisionShow,
} from '../actions/televisionShow';

const defaultError = 'Could not retrieve the show you are looking for';

export default store => next => async action => {
  next(action);

  if (action.type !== REQUEST_TELEVISION_SHOW) return;

  const { id } = action;

  try {
    const response = await fetch(`https://api.tvmaze.com/shows/${id}`);
    switch (response.status) {
      case 200: {
        const data = await response.json();
        store.dispatch(receiveTelevisionShow(data));
        break;
      }
      case 429: {
        throw new Error('Please try again in a few seconds');
      }
      default:
        throw new Error();
    }
  } catch (err) {
    store.dispatch(receiveTelevisionShow(null, err.message || defaultError));
  }
};
