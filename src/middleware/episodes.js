import { REQUEST_EPISODES, receiveEpisodes } from '../actions/episodes';

export default store => next => async action => {
  next(action);

  if (action.type !== REQUEST_EPISODES) return;
  const { showId } = action;

  try {
    const response = await fetch(
      `https://api.tvmaze.com/shows/${showId}/episodes?specials=1`
    );
    if (response.status === 200) {
      const data = await response.json();
      store.dispatch(receiveEpisodes(data, showId));
    } else {
      throw new Error();
    }
  } catch (err) {
    store.dispatch(receiveEpisodes(null, showId));
  }
};
