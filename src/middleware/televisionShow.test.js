import configureMockStore from 'redux-mock-store';
import televisionShow from './televisionShow';
import {
  RECEIVE_TELEVISION_SHOW,
  requestTelevisionShow,
} from '../actions/televisionShow';

describe('TelevisionShow middleware', () => {
  const mockStore = configureMockStore([televisionShow]);
  const store = mockStore({});

  beforeEach(() => {
    store.clearActions();
    fetch.resetMocks();
  });

  const televisionShowId = 6771;

  it('should succeed: 200', async () => {
    const apiMockData = { id: televisionShowId, summary: 'foo bar' };

    fetch.mockResponseOnce(JSON.stringify(apiMockData), { status: 200 });
    await store.dispatch(requestTelevisionShow(televisionShowId));
    const successAction = store
      .getActions()
      .find(({ type }) => type === RECEIVE_TELEVISION_SHOW);

    expect(successAction.type).toEqual(RECEIVE_TELEVISION_SHOW);
    expect(successAction.data).toEqual(apiMockData);
    expect(successAction.error).toEqual(undefined);
  });

  it('should succeed: 429', async () => {
    fetch.mockResponseOnce(JSON.stringify({}), { status: 429 });
    await store.dispatch(requestTelevisionShow(televisionShowId));
    const successAction = store
      .getActions()
      .find(({ type }) => type === RECEIVE_TELEVISION_SHOW);

    expect(successAction.type).toEqual(RECEIVE_TELEVISION_SHOW);
    expect(successAction.data).toEqual(null);
    expect(successAction.error).toEqual('Please try again in a few seconds');
  });

  it('should succeed: 500', async () => {
    fetch.mockResponseOnce(JSON.stringify({}), { status: 500 });
    await store.dispatch(requestTelevisionShow(televisionShowId));
    const successAction = store
      .getActions()
      .find(({ type }) => type === RECEIVE_TELEVISION_SHOW);
    expect(successAction.type).toEqual(RECEIVE_TELEVISION_SHOW);
    expect(successAction.data).toEqual(null);
    expect(successAction.error).toEqual(
      'Could not retrieve the show you are looking for'
    );
  });

  it("should succeed: can't fetch data", async () => {
    fetch.mockRejectOnce(JSON.stringify({}));
    await store.dispatch(requestTelevisionShow(televisionShowId));
    const successAction = store
      .getActions()
      .find(({ type }) => type === RECEIVE_TELEVISION_SHOW);
    expect(successAction.type).toEqual(RECEIVE_TELEVISION_SHOW);
    expect(successAction.data).toEqual(null);
    expect(successAction.error).toEqual(
      'Could not retrieve the show you are looking for'
    );
  });
});
