import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { requestEpisodes } from '../actions/episodes';
import EpisodeList from '../components/EpisodeList';

const mapStateToProps = (state, props) => {
  const { showId } = props.match.params;
  const item = state.episodes.list.find(episode => episode.showId === +showId);
  return {
    episodes: item && item.episodes,
    showId,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { showId } = props.match.params;

  return {
    getEpisodes: () => dispatch(requestEpisodes(showId)),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EpisodeList)
);
