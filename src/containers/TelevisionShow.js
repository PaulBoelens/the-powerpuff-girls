import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import TelevisionShow from '../components/TelevisionShow';
import { requestTelevisionShow } from '../actions/televisionShow';

const mapStateToProps = (state, props) => {
  const { showId } = props.match.params;
  return {
    show: state.televisionShow.list.find(({ id }) => id === +showId),
    episodes: [],
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { showId } = props.match.params;
  return {
    getShow: () => dispatch(requestTelevisionShow(showId)),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TelevisionShow)
);
