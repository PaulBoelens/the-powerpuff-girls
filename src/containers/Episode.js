import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { requestEpisodes } from '../actions/episodes';
import Episode from '../components/Episode';

const mapStateToProps = (state, props) => {
  const { showId, season, episode } = props.match.params;
  const item = state.episodes.list.find(o => o.showId === +showId);

  const episodeItem =
    item &&
    item.episodes &&
    item.episodes.find(o => o.season === +season && o.number === +episode);

  return {
    showId,
    episode: episodeItem,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { showId } = props.match.params;
  return {
    getEpisodes: () => dispatch(requestEpisodes(showId)),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Episode)
);
