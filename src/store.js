import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import rootMiddleware from './middleware';

const initialState = {};
const enhancers = [];

if (process.env.NODE_ENV === 'development') {
  const { __REDUX_DEVTOOLS_EXTENSION__ } = window;

  if (typeof __REDUX_DEVTOOLS_EXTENSION__ === 'function') {
    enhancers.push(__REDUX_DEVTOOLS_EXTENSION__());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...rootMiddleware),
  ...enhancers
);

export default createStore(rootReducer, initialState, composedEnhancers);
